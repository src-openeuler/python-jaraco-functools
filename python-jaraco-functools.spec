%global _empty_manifest_terminate_build 0
Name:           python-jaraco-functools
Version:        4.0.2
Release:        1
Summary:        Functools like those found in stdlib
License:        MIT
URL:            https://pypi.org/project/jaraco.functools/
Source0:        https://files.pythonhosted.org/packages/03/b1/6ca3c2052e584e9908a2c146f00378939b3c51b839304ab8ef4de067f042/jaraco_functools-4.0.2.tar.gz
BuildArch:      noarch

Requires:       python3-more-itertools
Requires:       python3-sphinx
Requires:       python3-pytest
Requires:       python3-pytest-checkdocs
Requires:       python3-pytest-flake8
Requires:       python3-pytest-black
Requires:       python3-pytest-cov
Requires:       python3-toml

%description
Functools like those found in stdlib

%package -n python3-jaraco-functools
Summary:        Functools like those found in stdlib
Provides:       python-jaraco-functools = %{version}-%{release}
BuildRequires:  python3-devel
BuildRequires:  python3-pip python3-wheel python3-flit
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-toml
%description -n python3-jaraco-functools
Functools like those found in stdlib

%package help
Summary:        Development documents and examples for jaraco.functools
Provides:       python3-jaraco-functools-doc
%description help
Documentation for jaraco-functools

%prep
%autosetup -n jaraco_functools-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-jaraco-functools
%{python3_sitelib}/*

%files help
%{_docdir}/*

%changelog
* Thu Oct 24 2024 dongqi1 <dongqi1@kylinos.cn> - 4.0.2-1
- Update package to version 4.0.2
- Enforce ruff/Perflint rule PERF401

* Wed May 08 2024 zhaojingyu <zhaojingyu@kylinos.cn> - 4.0.1-1
- Update package to version 4.0.1
- Remove pins for resolved issues

* Wed Feb 21 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 4.0.0-1
- Update package to version 4.0.0

* Tue Jul 04 2023 chenzixuan <chenzixuan@kylinos.cn> - 3.8.0-1
- Update package to version 3.8.0

* Fri Apr 28 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.6.0-2
- Use pyproject to compile package

* Thu Mar 30 2023 wubijie <wubijie@kylinos.cn> - 3.6.0-1
- Update package to version 3.6.0

* Mon Feb 27 2023 yaoxin <yaoxin30@h-partners.com> - 3.5.2-3
- Fix installation conflicts between python3-jaraco-functools and python3-tempora

* Fri Feb 24 2023 yaoxin <yaoxin30@h-partners.com> - 3.5.2-2
- Fix installation conflicts between python3-jaraco-functools and python3-importlib-metadata

* Thu Dec 01 2022 liqiuyu <liqiuyu@kylinos.cn> - 3.5.2-1
- Update package to version 3.5.2

* Sat Apr 02 2022 xigaoxinyan <xigaoxinyan@huawei.com> - 3.0.1-3
- Fix build error caused by py3.10+ wildcard

* Thu Dec 24 2020 zhanghua <zhanghua40@huawei.com> - 3.0.1-2
- fix random build failed

* Fri Nov 13 2020 Python_Bot <Python_Bot@openeuler.org> - 3.0.1-1
- Package Spec generated
